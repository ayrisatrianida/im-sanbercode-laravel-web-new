1. Membuat Database
   CREATE DATABASE myshop;

2. Membuat Table di Dalam Database
   CREATE TABLE users( 
	id int(8) PRIMARY KEY AUTO_INCREMENT, 
	name varchar(255) NOT Null, 
	email varchar(255), 
	password varchar(255) 
   );
   
   CREATE TABLE categories( 
	id INTEGER(8) PRIMARY KEY AUTO_INCREMENT, 
	name varchar(255) 
   );

   CREATE TABLE items( 
	id int(8) PRIMARY KEY AUTO_INCREMENT, 
	name varchar(255), 
	description varchar(255), 
	price int, stock int, 
	category_id int NOT null, 
	foreign key(category_id) references categories(id) 
   );

3. Memasukkan Data pada Table 
   USERS
    INSERT INTO users(name,email,password) 
	VALUES("John Doe","john@doe.com","john123"), 
	("Jane Doe","jane@doe.com","jenita123");
   CATEGORIES
    INSERT INTO categories(name) 
	VALUES("gadget"),("cloth"),("men"), ("women"), ("branded");
   ITEMS
    INSERT INTO items(name,description,price,stock,category_id) 
	VALUES("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),
	("Uniklooh","baju keren dari brand ternama",500000,50,2),
	("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Mengambil Data dari Database
   a. Mengambil data users
	SELECT id,name,email FROM users;
   b. Mengambil data items
	- SELECT * FROM items WHERE price > 1000000;
	- SELECT * FROM items WHERE name LIKE 'uniklo%';
	  SELECT * FROM items WHERE name LIKE '%sang%';
          SELECT * FROM items WHERE name LIKE '%watch';
   c. Menampilkan data items join dengan kategori
	SELECT items.*,categories.name FROM items LEFT JOIN categories ON items.category_id = categories.id;

5. Mengubah Data dari Database
	UPDATE items SET price = '2500000' WHERE name = 'sumsang b50';
   
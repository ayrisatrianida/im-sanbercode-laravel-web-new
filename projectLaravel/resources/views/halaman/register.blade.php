@extends('layout.master')

@section('title')
<h2>Buat Account Baru!</h2>
@endsection

@section('content')
<h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name</label> <br>
        <input type="text" name = "fname" > <br><br>
        <label >Last Name</label> <br>
        <input type="text" name = "lname" > <br><br>
        <label> Gender</label><br>
        <input type="radio" name = "gender"> Male <br>
        <input type="radio" name = "gender"> Female <br>
        <input type="radio" name = "gender"> Other <br> <br>
        <label >Nationality</label> <br> <br>
        <select name="Nationality"> 
            <option value="">Indonesia</option>
            <option value="">Malaysia</option>
            <option value="">Thailand</option>
            <option value="">Vietnam</option>
        </select> <br> <br>
        <label >Language Spoken : </label> <br>
        <input type="checkbox" name="Language Spoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken"> English <br>
        <input type="checkbox" name="Language Spoken"> Other <br> <br>
        <label >Bio</label> <br>
        <textarea cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
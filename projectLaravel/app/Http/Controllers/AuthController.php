<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view ('halaman.register');
    }

    public function akhir(Request $request)
    {
        $namaDepan = $request->input('fname');
        $namaBelakang = $request->input('lname');

        return view('halaman.welcome',["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);
    }
}
